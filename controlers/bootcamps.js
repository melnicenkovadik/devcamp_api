//  @description    Получить все лагери
//  @route          GET /api/v1/bootcamps
//  @access         Public
exports.getBootcamps = (req, res, next) => {
    res
        .status(200)
        .json({success: true, msg: 'SHOW ALL BOOTCAMPS'})
}

//  @description    Получить один лагерь
//  @route          GET /api/v1/bootcamps/:id
//  @access         Public
exports.getBootcamp = (req, res, next) => {
    res
        .status(200)
        .json({success: true, msg: `GET BOOTCAMP ${req.params.id}`})

}

//  @description    Создать новый лагерь
//  @route          POST /api/v1/bootcamps/:id
//  @access         Private
exports.createBootcamp = (req, res, next) => {
    res
        .status(200)
        .json({success: true, msg: 'CREATE NEW BOOTCAMP'})
}

//  @description    Обновить  лагерь
//  @route          PUT /api/v1/bootcamps/:id
//  @access         Private
exports.updateBootcamp = (req, res, next) => {
    res
        .status(200)
        .json({success: true, msg: `UPDATE BOOTCAMP ${req.params.id} `})
}

//  @description    Удалить  лагерь
//  @route          Delete /api/v1/bootcamps/:id
//  @access         Private
exports.deleteBootcamp = (req, res, next) => {
    res
        .status(200)
        .json({success: true, msg: `DELETE BOOTCAMP ${req.params.id} `})
}
