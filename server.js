const express = require('express')
const dotenv = require('dotenv')
const morgan = require('morgan')

//Файл с Роутами
const bootcamps = require('./routes/bootcamps');

//Подгружаю env переменные
dotenv.config({path: './config/config.env'});

const app = express();

// Логгер для разработки
if (process.env.NODE_ENV === 'development') {
    app.use(morgan("dev"))
}

//Монтирую роуты
app.use('/api/v1/bootcamps', bootcamps);


const PORT = process.env.PORT || 5000;


app.listen(
    PORT,
    console.log('\x1b[36m%s\x1b[0m', ` ☕☕☕☕ РАН ВАСЯ, РАН  on http://localhost:${PORT} ☕☕☕☕
    \x1b[33m  ☕☕☕☕   ${process.env.NODE_ENV.toLocaleUpperCase()}  MODE ☕☕☕☕`)
);

